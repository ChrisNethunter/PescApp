process.env.MAIL_URL="smtp://social%40vifuy.com:vifuyvifuy1234@smtp.gmail.com:465/"; 
Meteor.startup(function () {
	Meteor.call('initialD');

	/*smtp = {
	    username: 'your_username',   // eg: server@gentlenode.com
	    password: 'your_password',   // eg: 3eeP1gtizk5eziohfervU
	    server:   'smtp.gmail.com',  // eg: mail.gandi.net
	    port: 25
	}

	process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;*/

});

Meteor.publish('users', function(){
  	return Meteor.users.find();
});

Meteor.publish('userInfo', function(user){
  	return Meteor.users.find({_id:user ,"profile.status":true});
});

Meteor.publish('auctionsProducerUser', function(user){
  	return Auctions.find({createdBy:user});
});

Meteor.publish('auctions', function(){
  	return Auctions.find({});
});

Meteor.publish('auction', function(id){
  	return Auctions.find({_id:id});
});

Meteor.publish('offers-auction', function(auction){
  	return offers.find({auction:auction,status:"activo"});
});

Meteor.publish('offers-auction-producer', function(auction){
    return offers.find({auction:auction});
});

Meteor.publish('offers-user', function(user){
  	return offers.find({user:user});
});

Meteor.publish('offer', function(id){
  	return offers.find({_id:id});
});

Meteor.publish('statusOffers', function(id){
  	return statusOffers.find({userProducer:id});
});