Meteor.methods({
	initialD : function(){
		if (!Migrations.findOne({slug: 'users'})) {
			var users = [
			    {name:"Admin User",email:"social@vifuy.com", pass:"vifuyw4515991" , roles:['admin']},
			    {name:"Admin William ",email:"william@vifuy.com", pass:"w4515991" , roles:['admin']},
			    {name:"Admin Alejo ",email:"alejandro@vifuy.com" ,pass:"vifuy1234" , roles:['admin']},
				{name:"Admin Christian",email:"cristian@vifuy.com", pass:"vifuychobits" , roles:['admin']},
				{name:"Admin Alex",email:"alex@vifuy.com", pass:"" , pass:"vifuy1234" ,  roles:['admin']},
			];
			
			_.each(users, function (user) {
			  var id;

			  id = Accounts.createUser({
			    email: user.email,
			    password: user.pass,
			    profile: { name: user.name }
			  });

			  if (user.roles.length > 0) {
			    // Need _id of existing user record so this call must come 
			    // after `Accounts.createUser` or `Accounts.onCreate`
			    Roles.addUsersToRoles(id, user.roles);
			  }
			});

			Migrations.insert({slug: 'users'});
		}
	},

	'createUserProducer': function(data){

		var user = Meteor.users.find({"profile.cc":data.cc}).count()
		if (user != 1){
			id = Accounts.createUser({
				email: data.email,
			    password: data.password,
		    	profile:{ 
		    				name: data.fullname,
		    				cc: data.cc,
		    				phone: data.phone,
		    				email:data.email,
		    				address:data.address,
	      					production:data.production,
	      					cuts:data.cuts,
	      					video:data.video,
	      					photo:data.photo, 
	      					status: data.status,

		    			}			      				
		    });
			return id
		}else{
			return 2
		};
	},

	'createUserBuyer': function(data){

		var user = Meteor.users.find({"profile.cc":data.cc}).count()
		if (user != 1){
			id = Accounts.createUser({
				email: data.email,
			    password: data.password,
		    	profile:{ 
		    				name: data.fullname,
		    				cc: data.cc,
		    				phone: data.phone,
		    				email:data.email,
		    				address:data.address,
	      					status: data.status,

		    			}			      				
		    });
			return id
		}else{
			return 2
		};
	},

	'createUserWithRole': function(data) {
	    var userId;
	    var role = ['free'];
	    var email = data.email;

	    var user = Meteor.users.find({"emails.address":email}).count()

	    if(user == 1){
	    	return 3
	    }else{
	    
	    	Meteor.call('createUserNoRole', data, function(err, result) {
		      if (err) {
		        return err;
		      }

		      Roles.addUsersToRoles(result, role);
		      return userId = result;

			});
			return userId;
	    };    
	},

	'createUserNoRole': function(data) {
	    //Do server side validation
	    return Accounts.createUser({
	      email: data.email,
	      password: data.password,
	      profile: data.profile
	    });
	},

	'createAuction': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    Auctions.insert(data);
	},

	'updateAuction': function(data) {
	    //Do server side validation
	    if (! Meteor.userId() && Meteor.userId() == data.user) {
	      throw new Meteor.Error("not-authorized");
	    }

	    if (data.finish == "ON"){
	    	Auctions.update({'_id':data._id},{$set:{"dateHarvest":data.dateHarvest,"dateInit":data.dateInit,"dateFinish":data.dateFinish,
		    	"value":data.value,"notes":data.notes,"weight":data.weight,"status":"finalizada",dateModify:data.dateModify,timeModify:data.timeModify,finish:data.finish
			}});
	    }else{
	    	Auctions.update({'_id':data._id},{$set:{"dateHarvest":data.dateHarvest,"dateInit":data.dateInit,"dateFinish":data.dateFinish,
		    	"value":data.value,"notes":data.notes,"weight":data.weight,"status":data.status,dateModify:data.dateModify,timeModify:data.timeModify,finish:data.finish
			}});
	    };
	    
	},


	'createOffer': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    offers.insert(data);
	},



	'updateProducer': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    if (data.photo == ''){
	    	Meteor.users.update({'_id':data._id},{$set:{"profile.name":data.name, "profile.phone":data.phone,"profile.email":data.email,
							"profile.address":data.address, "profile.production":data.production, "profile.cuts":data.cuts,
							"profile.video":data.video,"emails":[{address:data.email,verified:false}]
			}});
	    }else{
	    	Meteor.users.update({'_id':data._id},{$set:{"profile.name":data.name, "profile.phone":data.phone,"profile.email":data.email,
							"profile.address":data.address, "profile.production":data.production, "profile.cuts":data.cuts,
							"profile.video":data.video, "profile.photo":data.photo,"emails":[{address:data.email,verified:false}]
			}});

	    };

		
	},

	'updateBuyer':function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

	    Meteor.users.update({'_id':data._id},{$set:{"profile.name":data.name, "profile.phone":data.phone,"profile.email":data.email,
						"profile.address":data.address,"emails":[{address:data.email,verified:false}]
		}});

	},

	'newPassword': function(password) {
		Accounts.resetPassword(Session.get('resetPassword'), password, function(err) {
            if (err) {
            	return 2
              	//console.log('We are sorry but something went wrong.');
            } else {
              	//console.log('Your password has been changed. Welcome back!');
              	Session.set('resetPassword', null);
              	return 1
            }
        });
	    
	},

  	setRoleToSellerProducer : function(id){
		Roles.addUsersToRoles(id, ['producer']);
	},
	setRoleToBuyer : function(id){
		Roles.addUsersToRoles(id, ['buyer']);
	},
  	setRoleToFreeUser : function(idU){
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['freeUser']);
	},
	setRoleToPlatinumUser : function(idU){
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['platinumUser']);
	},
	setRoleToGoldUser : function(idU){
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['goldUser']);
	},
	updateRoleToFreeUser : function(idU){
		Roles.setUserRoles(idU, ['freeUser']);
	},
	updateRoleToPlatinumUser : function(idU){
		Roles.setUserRoles(idU, ['platinumUser']);
	},
	updateRoleToGoldUser : function(idU){
		Roles.setUserRoles(idU, ['goldUser']);
	},
	updateRoleXToUser : function(data){
		Roles.setUserRoles(data.uId,data.rol);
		//return ('it\'s done boy');
	},

	'changeOffer': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

	    offers.update({_id:data.id},{$set:{"offer":data.offer,"weight":data.weight,"modifyAt":data.modifyAt, "modifyTimeCreater":data.modifyTimeCreater }} );
	},

	'cancelOfferBuyer': function(id) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

	    offers.update({_id:id},{$set:{status:"inactivo"}} );
	},

	'offerAccept': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

	    statusOffers.remove({auctionId:data.auctionId,status:"canceled"})
	    if (statusOffers.find({auctionId:data.auctionId ,status:true}).count() == 1){
	    	return 2
	    }else{
	    	offers.update({_id:data.offerId},{$set:{status:"finalizada"}} );
	    	statusOffers.insert(data);
	    };
	    
	},

	'offerCancel': function(data) {
		dataStatusOffer = statusOffers.findOne({userProducer:data.userId,auctionId:data.auctionId})
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

	    if (dataStatusOffer.status == "canceled"){
	    	return 2
	    }else{
	    	offers.update({_id:data.offerId},{$set:{status:"activo"}} );
	    	statusOffers.update({_id:dataStatusOffer._id},{$set:{dateCancel:data.dateCancel,timeCancel:data.timeCancel,status:"canceled"}})
	    };
	    
	    
	},

	sendEmailConfirmationOffer:function(dataProducer,to){
		Email.send({
		  from: dataProducer.emails[0].address,
		  to: to,
		  subject: "Oferta Aceptada",
		  text: 'El Productor "'+dataProducer.profile.name+'"  ha aceptado tu oferta porfavor, comunicate con el atraves de este mismo medio para concretar la compra. \
				\n 	Datos Productor : \n \
				\n	Nombre : "'+dataProducer.profile.name+'" \n \
				\n  C.C: "'+dataProducer.profile.cc+'" \n \
				\n	Teléfono : "'+dataProducer.profile.phone+'"\n \
				\n	Correo: "'+dataProducer.profile.email+'"\n \
				\n	Dirección: "'+dataProducer.profile.address+'" \n ' 
		});
	},

	sendEmailConfirmationOfferCancel:function(dataProducer,to){
		Email.send({
		  from: dataProducer.emails[0].address,
		  to: to,
		  subject: "Oferta Cancelada",
		  text: 'El Productor "'+dataProducer.profile.name+'"  ha cancelado tu oferta porfavor, comunicate con el atraves de este mismo medio. \
				\n 	Datos Productor : \n \
				\n	Nombre : "'+dataProducer.profile.name+'" \n \
				\n  C.C: "'+dataProducer.profile.cc+'" \n \
				\n	Teléfono : "'+dataProducer.profile.phone+'"\n \
				\n	Correo: "'+dataProducer.profile.email+'"\n \
				\n	Dirección: "'+dataProducer.profile.address+'" \n ' 
		});
	},


})