Router.configure({
	layoutTemplate: 'loginUser',  
});

Router.map(function(){
	this.route('home', {
		path: '/',
		layoutTemplate: 'loginUser',

	});	
	this.route('singUp', {
		path: '/singUp',
		layoutTemplate: 'signUp',
	});
	this.route('singUpProducer', {
		path: '/singUp',
		layoutTemplate: 'producer_register',
	});	
	this.route('singUpBuyer', {
		path: '/singUp',
		layoutTemplate: 'buyer_register',
	});		
	this.route('signIn', {
		path: '/signIn',
		layoutTemplate: 'loginUser',
	});

	/*ADMIN PRODUCER*/
	this.route('dashboard', {
		path: '/dashboard',
		layoutTemplate: 'dashboard',
	});
	this.route('createAuction', {
		path: '/crearSubasta',
		layoutTemplate: 'createAuction',
	});
	this.route('offer-auction-producer', {
		path: '/oferta/:_Id',
		layoutTemplate: 'offers_auction',
		waitOn	: function () {
			Meteor.subscribe('auction',this.params._Id);	
		},
		data:function(){
			return Auctions.findOne({_id:this.params._Id})
		},	
	});
			
	this.route('auctions', {
		path: '/subastas',
		layoutTemplate: 'dashboard',
	});
	this.route('update-auction', {
		path: '/subasta/:_Id',
		layoutTemplate: 'updateAuction',
		waitOn	: function () {
			Meteor.subscribe('auction',this.params._Id);	
		},
		data:function(){
			return Auctions.findOne({_id:this.params._Id})
		},	
	});
	this.route('profile-producer', {
		path: '/perfil/:_Id',
		layoutTemplate: 'profile_producer',
		waitOn	: function () {
			Meteor.subscribe('userInfo',this.params._Id);	
		},
		data:function(){
			return Meteor.users.findOne({_id:this.params._Id})
		},	
	});

	/*ADMIN PRODUCER*/

	/*ADMIN BUYER*/
	this.route('offer-auction', {
		path: '/oferta/:_Id',
		layoutTemplate: 'offer_auction',
		waitOn	: function () {
			Meteor.subscribe('auction',this.params._Id);	
		},
		data:function(){
			return Auctions.findOne({_id:this.params._Id})
		},	
	});

	this.route('auctions-buyer', {
		path: '/subastas',
		layoutTemplate: 'dashboard',
	});

	this.route('profile-buyer', {
		path: '/profile/:_Id',
		layoutTemplate: 'profile_buyer',
		waitOn	: function () {
			Meteor.subscribe('userInfo',this.params._Id);	
		},
		data:function(){
			return Meteor.users.findOne({_id:this.params._Id})
		},	
	});	
	this.route('offers', {
		path: '/ofertas',
		layoutTemplate: 'offers',
	});

	this.route('my-offer-data', {
		path: '/mioferta/:_Id',
		layoutTemplate: 'my_offers_buyer',
		waitOn	: function () {
			Meteor.subscribe('offer',this.params._Id);	
		},
		data:function(){
			return offers.findOne({_id:this.params._Id})
		},	
	});

	this.route('offerBuyerInfo', {
		path: '/oferta/:_Id',
		layoutTemplate: 'offer_auction_producer',
		waitOn	: function () {
			Meteor.subscribe('offer',this.params._Id);
		},
		data:function(){
			return offers.findOne({_id:this.params._Id}) 
		},	
	});
		

});

