if (Meteor.isClient) {
    Template.dashboard_menu.events({
    	'click #logout': function(event){
	        event.preventDefault();
	        Meteor.logout();
	    },
	    'click #auctions-buyer-offers': function(event){
	        event.preventDefault();
	        Router.go('offers');
	    },
	    'click #auctions': function(event){
	        event.preventDefault();
	        Router.go('auctions');
	    },
	    'click #profile-producer': function(event){
	        event.preventDefault();
	        Router.go('profile-producer',{_Id:Meteor.userId()});
	    },
	     'click #profile-buyer': function(event){
	        event.preventDefault();
	        Router.go('profile-buyer',{_Id:Meteor.userId()});
	    },
	    'click #auctions-buyer': function(event){
	        event.preventDefault();
	        Router.go('auctions-buyer');
	    },
	    'click #main-menu-toggle': function(event){
	        event.preventDefault();
	        if ($('body').hasClass("mmc")){
	        	$('body').removeClass( "mmc" )
	        	$('body').addClass('mme');
	        }else{
	        	$('body').removeClass( "mme" )
	        	$('body').addClass('mmc');
	        };
	    },

    
    });

    Template.dashboard_menu.rendered = function(){
		
	}
}

