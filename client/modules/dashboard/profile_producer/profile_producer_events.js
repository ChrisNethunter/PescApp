if (Meteor.isClient) {
  user = Meteor.userId();

  Template.profile_producer.events({
  	'click #create-new-auction': function (event) {
        event.preventDefault();
		    Router.go('createAuction');    
    },
    'click #photo': function () {
      event.preventDefault();
      var cameraOptions = {
          width: 800,
          height: 600
      };
      MeteorCamera.getPicture(cameraOptions,function(err, res){
        if (!err){
          Session.set("picture" , res);
        };
      })
    },
    'submit #signUp-form_id_producer' : function(event, template) {
      event.preventDefault();
      $(".alert").remove();
      var username = template.find('#name_id_producer').value;
      var address =template.find('#address_producer').value;
      var email = template.find('#email_id_producer').value;
      var phone=template.find('#phone_id_producer').value;
      var production=template.find('#production_id').value;
      var cuts=template.find('#cuts_id').value;
      var video=template.find('#url_video_producer').value;
      var photo = Session.get("picture");

      updateProfileProducer(username,address,email,phone,production,cuts,video,photo)      
    },
    'submit #change-password' : function(event, template) {
      event.preventDefault();
      $(".alert").remove();      
      var currentPassword = template.find('#current-password').value;
      var newPassword = template.find('#new-password').value;
      var repetNewPassword = template.find('#new-password-repet').value;

      newPasswordMethod(currentPassword,newPassword,repetNewPassword)      
    }

  	
  });

  Template.dashboard.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('auctionsProducerUser',Meteor.userId());

  });

  Template.dashboard.rendered = function(){
    //Router.go('dashboardMap'); 
    
    //$('body').removeClass( "page-signup" )
  }
}

function updateProfileProducer(username,address,email,phone,production,cuts,video,photo){
  $(".alert").remove()

  if (validateEmail(email) && isNotEmpty(username) && isNotEmpty(username) &&
    isNotEmpty(address) && validateNumber(phone) && isNotEmpty(production) && isNotEmpty(cuts) && isUrl(video) === true ){// &amp;&amp; other validations) {

    var data =  {
                  _id:$("#id").val(),
                  email:email,
                  name:username,
                  email:email,
                  phone:phone,
                  address:address,
                  production:production,
                  cuts:cuts,
                  video:video,
                  photo:'',

              };

    if (photo != ''){
      data.photo = photo
    }
    Meteor.call('updateProducer',data, function(err, res){
      if (err){
        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo</h4>';
        $(liData).appendTo('.alert-add').fadeIn('slow');
      }else{
        var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Modificado Correctamente</h4>';
        $(liData).appendTo('.alert-add').slideDown();
      };
    });
      
    // Then use the Meteor.createUser() function*/
  }else{
    var liData = '<h4 id="alert" class="alert" style="color:red;display:none;" align="center">Rellena todos los Campos Correctamente</h4>';
        $(liData).appendTo('.alert-add').slideDown();
  }
}



