if (Meteor.isClient) {
	// counter starts at 0
	Template.offer_auction.events({
		'click #cancelar': function () {
			event.preventDefault();
			Router.go('dashboard');    
		},
		'submit #offer-auction-buyer': function(event, template) {
			event.preventDefault();
			insertOffer($('#offer').val(),$('#weight').val(),$("#id-auction").val());
		},
	});

	Template.offer_auction.onCreated(function () {
	    // Use this.subscribe inside onCreated callback
	    Meteor.subscribe('users');

	});

	Template.offer_auction.rendered = function(){
		setTimeout(function(){ 
			var idProducer = $("#producer").val();
		    //Meteor.subscribe('userInfo',idProducer);

		    var dataProducer = Meteor.users.findOne({_id:idProducer});


		    $("#cc-producer").append("C.C "+dataProducer.profile.cc);
		    $("#phone-producer").append("Teléfono: "+dataProducer.profile.phone);
		    $("#address-producer").append("Direccion: "+dataProducer.profile.address);
		    $("#email-producer").append("Email: "+dataProducer.profile.email);
		    $("#production-producer").append("Producción: "+dataProducer.profile.production);
		    $("#cuts-producer").append("Cortes: "+dataProducer.profile.cuts);
		    $("#video-producer").append("Video: "+dataProducer.profile.video);
		    $(".imgProducer").attr("src", dataProducer.profile.photo);
		}, 500);
	}


	function insertOffer(offer,weight,idAuction){
		$(".alert").remove()
		var date = dateString();
		var idProducer = $("#producer").val();
		if (validateNumber(offer) && validateNumber(weight) === true){
			var data = 	{
							user:Meteor.userId(),
							userCreateOffer:Meteor.user().profile.name,
							createAt:date["stringDate"],
							timeCreater:date["time"],
							producer:idProducer,
							nameCreateAuction:$("#producer-name").val(),
							weight:weight,
							auction:idAuction,
							offer:offer,
							status:'activo'

						} 
			Meteor.call('createOffer',data, function(err, res){
		      if (err){
		        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo</h4>';
		        $(liData).appendTo('.alert-offer-insert').fadeIn('slow');
		      }else{
		        var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Insertado Correctamente</h4>';
		        $(liData).appendTo('.alert-offer-insert').slideDown();
		      };
	    	});				
		};

	}
}





