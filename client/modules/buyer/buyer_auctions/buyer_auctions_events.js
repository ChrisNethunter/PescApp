if (Meteor.isClient) {
  user = Meteor.userId();

  Template.buyer_auctions.events({
  	'click .offer-auction': function (event) {
        event.preventDefault();
		    Router.go('offer-auction',{_Id:event.currentTarget.id});  
    },
  	
  });

  Template.buyer_auctions.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('auctions');

  });

  Template.buyer_auctions.rendered = function(){
    
  }
}


