if (Meteor.isClient) {
  // counter starts at 0
  Template.buyer_auctions.helpers({
  		auctions: function(object) {
	    	return Auctions.find();
	    },

	    tableSettings : function () {
	         return {
	                    rowsPerPage: 10,
	                    fields: [
	                        { 
	                            key: 'dateHarvest', 
	                            label: 'Fecha de Cosecha' ,

	                        },
	                        { 
	                            key: 'dateHarvest',
	                            label: 'Inicio' 

	                        },
	                        { 
	                            key: "dateFinish", 
	                            label: 'Finalizacion' 
	                        },
	                        { 
	                            key: "weight", 
	                            label: 'Peso' 

	                        },
	                        { 
	                            key: "notes", 
	                            label: 'Notas' 
	                        },
	                        { 
	                            key: "status", 
	                            label: 'Estado',
	                            fn: function (estado, object) {
	                            	if (estado == "activo"){
	                            		var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
	                            		
	                            	}else if(estado == "finalizada"){
	                            		var html =  '<div id="finalizada" class="btn btn-info btn-sm">Finalizado</div>';
	                            	}else{
	                            		var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
	                            	};
	                            	return new Spacebars.SafeString(html);
	                                
	                            }

	                        },
	                        { 
	                            key: '_id', 
	                            label: 'Opciones',
	                            fn: function (_id, object) {
	                                var html =  '<button id="'+_id+'" class="offer-auction">Ver</button>';
	                                return new Spacebars.SafeString(html);
	                            }

	                        },
	                       
	                    ],
	   
	        };
	    }
  });
}


