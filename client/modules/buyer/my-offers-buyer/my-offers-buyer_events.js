if (Meteor.isClient) {
	// counter starts at 0
	Template.my_offers_buyer.events({

		'click .cancel-offer': function (event) {
			event.preventDefault();
			cancelOffer($("#id-offer").val())    
		},
		'submit #offer-auction-buyer-change': function(event, template) {
			event.preventDefault();
			changeOffer($("#id-offer").val(),$('#offer').val(),$('#weight').val());
		},
	});

	Template.my_offers_buyer.onCreated(function () {
	    // Use this.subscribe inside onCreated callback
	    Meteor.subscribe('users');
	    Meteor.subscribe('auctions');
	});

	Template.my_offers_buyer.rendered = function(){
		setTimeout(function(){ 

			dataOffer=offers.findOne({_id:$("#id-offer").val()});
		    if (dataOffer.status == "finalizada"){
		    	$(".cancel-offer").remove()
		    };

			var idProducer = $("#producer").val();
			var auction = $("#id-auction").val();

		    var dataProducer = Meteor.users.findOne({_id:idProducer});
		    var dataAuction = Auctions.findOne();

		    $("#cc-producer").append("C.C "+dataProducer.profile.cc);
		    $("#phone-producer").append("Teléfono: "+dataProducer.profile.phone);
		    $("#address-producer").append("Direccion: "+dataProducer.profile.address);
		    $("#email-producer").append("Email: "+dataProducer.profile.email);
		    $("#production-producer").append("Producción: "+dataProducer.profile.production);
		    $("#cuts-producer").append("Cortes: "+dataProducer.profile.cuts);
		    $("#video-producer").append("Video: "+dataProducer.profile.video);
		    $(".producer-photo").attr("src" , dataProducer.profile.photo);

		    $("#date-harvest").append("Fecha de Cosecha "+dataAuction.dateHarvest);
		    $("#date-init").append("Fecha Inicio"+dataAuction.dateInit);
		    $("#date-finish").append("Fecha Finalizacion"+dataAuction.dateFinish);
		    $("#notes").append("Notas"+dataAuction.notes);
		    $("#weight-auction").append("Peso Kg"+dataAuction.weight);


		}, 1000);
	}

	function changeOffer(id,offer,weight){
		$(".alert").remove()

		var date = dateString();
		if (validateNumber(offer) && validateNumber(weight) === true){
			var data = 	{
							id:id,
							modifyAt:date["stringDate"],
							modifyTimeCreater:date["time"],
							weight:weight,
							offer:offer,
						} 
			Meteor.call('changeOffer',data, function(err, res){
		      if (err){
		        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo</h4>';
		        $(liData).appendTo('.alert-offer-insert').fadeIn('slow');
		      }else{
		        var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Modificado Correctamente</h4>';
		        $(liData).appendTo('.alert-offer-insert').slideDown();
		      };
	    	});				
		}else{
			alert("Rellena Todos los Campos")
		};
	}

	function cancelOffer(idOffer){
		$(".alert").remove()
		Meteor.call('cancelOfferBuyer',idOffer, function(err, res){
	      	if (err){
		        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo</h4>';
		        $(liData).appendTo('.alert-offer-insert').fadeIn('slow');
	      	}else{
	        	Router.go('dashboard');    	      
	       	};
    	});

	}
}





