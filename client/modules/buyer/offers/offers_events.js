if (Meteor.isClient) {
  user = Meteor.userId();

  Template.offers.events({
  	'click .offer-auction': function (event) {
        event.preventDefault();
		    Router.go('my-offer-data',{_Id:event.currentTarget.id});  
    },
  	
  });

  Template.offers.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('offers-user',Meteor.userId());

  });

  Template.offers.rendered = function(){
    
  }
}


