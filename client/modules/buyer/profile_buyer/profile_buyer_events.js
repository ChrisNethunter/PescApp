if (Meteor.isClient) {
  user = Meteor.userId();

  Template.profile_buyer.events({
  	'click #create-new-auction': function (event) {
        event.preventDefault();
		    Router.go('createAuction');    
    },

    'submit #signUp-form_id_buyer' : function(event, template) {
      event.preventDefault();
      $(".alert").remove();
      var username = template.find('#name_id_buyer').value;
      var address =template.find('#address_buyer').value;
      var email = template.find('#email_id_buyer').value;
      var phone=template.find('#phone_id_buyer').value;

      updateProfilebuyer(username,address,email,phone)      
    },

    'submit #change-password' : function(event, template) {
      event.preventDefault();
      $(".alert").remove();      
      var currentPassword = template.find('#current-password').value;
      var newPassword = template.find('#new-password').value;
      var repetNewPassword = template.find('#new-password-repet').value;

      newPasswordMethod(currentPassword,newPassword,repetNewPassword)      
    }
  	
  });

  Template.profile_buyer.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('auctionsbuyerUser',Meteor.userId());

  });

  Template.profile_buyer.rendered = function(){
    //Router.go('dashboardMap'); 
    
    //$('body').removeClass( "page-signup" )
  }
}

function updateProfilebuyer(username,address,email,phone){
  $(".alert").remove()

  if (validateEmail(email) && isNotEmpty(username) && isNotEmpty(username) &&
    isNotEmpty(address) && validateNumber(phone)  === true ){// &amp;&amp; other validations) {

    var data =  {
                  _id:$("#id").val(),
                  email:email,
                  name:username,
                  phone:phone,
                  address:address,
              };

    Meteor.call('updateBuyer',data, function(err, res){
      if (err){
        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo</h4>';
        $(liData).appendTo('.alert-add').fadeIn('slow');
      }else{
        var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Modificado Correctamente</h4>';
        $(liData).appendTo('.alert-add').slideDown();
      };
    });
      
    // Then use the Meteor.createUser() function*/
  }else{
    var liData = '<h4 id="alert" class="alert" style="color:red;display:none;" align="center">Rellena todos los Campos Correctamente</h4>';
        $(liData).appendTo('.alert-add').slideDown();
  }
}
newPasswordMethod = function(currentPassword,newPassword,repetNewPassword){

  if ( newPassword == repetNewPassword && validatePassword(document.getElementById("new-password"))  === true ){
    Accounts.changePassword(currentPassword, newPassword, function(error) {
      if (error) {
        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo "'+ error.reason +'"</h4>';
        $(liData).appendTo('.form-actions').fadeIn('slow');
         
      } else {
         var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Tu contraseña ha sido cambiada correctamente</h4>';
        $(liData).appendTo('.alert-add').slideDown();
      }
    });
  }else{
    var liData = '<h4 id="alert" class="alert" style="color:red;display:none;" align="center">Rellena Todos Los Campos</h4>';
    $(liData).appendTo('.alert-add').slideDown();
  }

}


