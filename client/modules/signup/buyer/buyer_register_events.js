if (Meteor.isClient) {
  // counter starts at 0
	Template.buyer_register.events({
	'click #signIn' : function(event, t){
	  event.preventDefault();
	  Router.go('signIn');    
	},

	'submit #signup-form_id_buyer' : function(event, template) {
	  	event.preventDefault();
	  	$(".alert").remove();

	  	var email = validateEmail(template.find('#email_id_buyer').value);
	  	var userPassword = validatePassword(template.find('#password_id_buyer'));
	  	var phone = validateNumber(template.find('#phone_id_buyer').value);
	  	var address = isNotEmpty(template.find('#address_id_buyer').value);
	  	var fullname = isNotEmpty(template.find('#name_id_buyer').value);
	  	var cc = validateNumber(template.find('#cc').value);
	  
	  	if ( userPassword &&  email && phone && fullname && phone && address === true ){// &amp;&amp; other validations) {
			var data =  {
		      				email:template.find('#email_id_buyer').value,
		      				password:template.find('#password_id_buyer').value,
	      					fullname:template.find('#name_id_buyer').value,
	      					address:template.find('#address_id_buyer').value,
	      					phone:template.find('#phone_id_buyer').value,
	      					cc:template.find('#cc').value,
	      					status:true,

		      				
		      			};


		    Meteor.call('createUserBuyer',data, function(err, res){
		  		//alert("email " + res.email +"  pass "+ res.password + " fullname "+ res.fullname + " username " + res.username)
		  		id = res;
		  		if(typeof(res) == 'string'){
		  			Meteor.call('setRoleToBuyer',id, function(err, res){
		  				if (err){
				            var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo "'+ err +'"</h4>';
	      			  		$(liData).appendTo('.form-actions').fadeIn('slow');
				        }else{
				           	var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Registrado Correctamente </h4>';
	      			  		$(liData).appendTo('.form-actions').slideDown();
				        };
		  			});
		  		}else if(res == 2){
		        	var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Este Usuario ya esta registrado</h4>';
      			  	$(liData).appendTo('.form-actions').fadeIn('slow');

		        }else{
		        	var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">Problemas Intenta de Nuevo "'+ err +'"</h4>';
      			  	$(liData).appendTo('.form-actions').slideDown();
		        }
			});
			// Then use the Meteor.createUser() function*/
		}else{
			var liData = '<h4 id="alert" class="alert" style="color:red;display:none;" align="center">Rellena Todos Los Campos</h4>';
      		$(liData).appendTo('.form-actions').slideDown();
		}
	
	  	return false;
	}
	});

	Template.buyer_register.rendered = function(){
		 $("body").addClass("page-signup");

	}

}


