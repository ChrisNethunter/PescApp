if (Meteor.isClient) {
  // counter starts at 0
	Template.producer_register.events({
	'click #signIn' : function(event, t){
	  event.preventDefault();
	  Router.go('signIn');    
	},
	'click #photo': function () {
      event.preventDefault();
      var cameraOptions = {
          width: 800,
          height: 600
      };
      MeteorCamera.getPicture(cameraOptions,function(err, res){
        if (!err){
          Session.set("picture" , res);
        };
      })
    },

	'submit #signup-form_id_producer' : function(event, template) {
	  	event.preventDefault();
	  	$(".alert").remove();


	  	var username = template.find('#name_id_producer').value;
	  	var cc = template.find('#cc_id_producer').value;
	  	var address =template.find('#address_producer').value;
	  	var email = template.find('#email_id_producer').value;
	  	var phone=template.find('#phone_id_producer').value;
	  	var userPassword = template.find('#password_id');
	  	var production=template.find('#production_id').value;
	  	var cuts=template.find('#cuts_id').value;
	  	var video=template.find('#url_video_producer').value;
	  	var photo = Session.get("picture");

	  	if ( validatePassword(userPassword) &&  validateEmail(email) && isNotEmpty(username) && isNotEmpty(username) &&
			isNotEmpty(address) && validateNumber(phone) && validateNumber(cc)&& isNotEmpty(production) && isNotEmpty(cuts) && isUrl(video) === true ){// &amp;&amp; other validations) {
			var data =  {
		      				email:email,
		      				password:template.find('#password_id').value,
	      					fullname:username,
	      					cc:cc,
	      					email:email,
	      					phone:phone,
	      					address:address,
	      					production:production,
	      					cuts:cuts,
	      					video:video,
	      					photo:photo,
	      					status:true,

		      				
		      			};


		    Meteor.call('createUserProducer',data, function(err, res){
				if(typeof(res) == 'string'){
		        	id = res;
			        Meteor.call('setRoleToSellerProducer',id, function(err, res){
			          	if (err){
				            var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas Intenta de Nuevo "'+ err +'"</h4>';
	      			  		$(liData).appendTo('.form-actions').fadeIn('slow');
				        }else{
				           	var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Registrado Correctamente </h4>';
	      			  		$(liData).appendTo('.form-actions').slideDown();
				        };
		        	});
		        }else if(res == 2){
		        	var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Este Usuario ya esta registrado</h4>';
      			  	$(liData).appendTo('.form-actions').fadeIn('slow');

		        }else{
		        	var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">Problemas Intenta de Nuevo "'+ err +'"</h4>';
	      			  		$(liData).appendTo('.form-actions').slideDown();
		        }
			});
			// Then use the Meteor.createUser() function*/
		}else{
			var liData = '<h4 id="alert" class="alert" style="color:red;display:none;" align="center">Rellena todos los Campos Correctamente</h4>';
      		$(liData).appendTo('.form-actions').slideDown();
		}
	
	  	return false;
	}

		

	});

	Template.producer_register.onCreated(function () {
    	// Use this.subscribe inside onCreated callback
    	delete Session.keys['picture'];
  	});

	Template.producer_register.rendered = function(){
		 $("body").addClass("page-signup");

	}

}


