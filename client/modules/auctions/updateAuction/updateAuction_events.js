if (Meteor.isClient) {
  // counter starts at 0
  Template.updateAuction.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('dashboard');    
    },
    'click .switcher-inner': function(event, template) {
      if ($('#status-auction').val()=="activo"){
        $('#status-auction').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
        $('#completeAuction').val("ON");

      }else{
        $('#status-auction').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
        $('#completeAuction').val("OFF");
      };
    },

    'click #completeAuction': function(event, template) {

      if ($("#completeAuction").val() == "OFF"){
        $('#completeAuction').val("ON");

        $('#status-auction').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");

      }else{
        $('#completeAuction').val("OFF");

        $('#status-auction').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
        
      }
    },

    'submit #updateAuction': function(event, template) {
        event.preventDefault();
        updateAuction($("#_id").val(),$("#createdBy").val(),$('#status-auction').val(),$('#inputDateHarvest').val(),$('#date-initial').val(),$('#date-finish').val(),$('#weight-production').val(),$('#notes-production').val(),$("#value-action").val(),$("#completeAuction").val());
    },
  });

  Template.updateAuction.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    //Meteor.subscribe('sellers');

  });

  Template.updateAuction.rendered = function(){
    if ($('#status-auction').val()=="activo"){
      $(".switcher-state-on").css("margin-left" , "10%");
      $(".switcher-toggler").css("margin-left" , "39px");
    };
  }

  function updateAuction(_id,createdBy,status,dateHarvest,dateInit,dateFinish,weight,notes,valueAuction,finishAuction){
    $(".alert").remove();
    if (validate_fecha(dateHarvest) && validate_fecha(dateInit) && validate_fecha(dateFinish) && 
        validateNumber(weight) && isNotEmpty(notes) && validateNumber(valueAuction) === true ){
      
      if (finishAuction != "ON"){
        $('#completeAuction').val("ON");

        $('#status-auction').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
       
      }else{
        $('#completeAuction').val("OFF");
        $('#status-auction').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
        
      }

      var date = dateString();
      data = {
        _id:_id,
        createdBy:createdBy,
        dateHarvest:dateHarvest,
        dateInit:dateInit,
        dateFinish:dateFinish,
        value:valueAuction,
        notes:notes,
        weight:weight,
        status:status,
        finish:finishAuction,
        dateModify:date["stringDate"],
        timeModify:date["time"],
      }

      Meteor.call('updateAuction',data, function(err, res){
        if (err){
          var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas, Intenta de Nuevo</h4>';
          $(liData).appendTo('.alert-add').fadeIn('slow');
        }else{
          var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Modificado Correctamente</h4>';
          $(liData).appendTo('.alert-add').slideDown();
        };
      });
    }else{
       var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Rellena los Campos Correctamente</h4>';
        $(liData).appendTo('.alert-add').fadeIn('slow');
    };
  }
}



