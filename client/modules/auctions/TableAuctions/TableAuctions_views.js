if (Meteor.isClient) {
  // counter starts at 0
  	Template.TableAuctions.helpers({
	    auctions: function(object) {
	    	return Auctions.find();
	    },

	    tableSettings : function () {
	         return {
	                    rowsPerPage: 10,
	                    fields: [
	                        { 
	                            key: 'dateHarvest', 
	                            label: 'Fecha de Cosecha' ,

	                        },
	                        { 
	                            key: 'dateHarvest',
	                            label: 'Inicio' 

	                        },
	                        { 
	                            key: "dateFinish", 
	                            label: 'Finalizacion' 
	                        },
	                        { 
	                            key: "weight", 
	                            label: 'Peso' 

	                        },
	                        {
	                        	key:"value",
	                        	label:"Valor-Mínimo",
	                        	fn: function (value, object) {
	                                var html =  '<td id="value" class="value">$'+value+'</td>';
	                                return new Spacebars.SafeString(html);
	                            }
	                        },
	                        { 
	                            key: "notes", 
	                            label: 'Notas' 
	                        },
	                        { 
	                            key: "status", 
	                            label: 'Estado',
	                            fn: function (estado, object) {
	                            	if (estado == "activo"){
	                            		var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
	                            	}else if(estado == "finalizada"){
	                            		var html =  '<div id="finalizada" class="btn btn-info btn-sm">Finalizado</div>';
	                            	}else{
	                            		var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
	                            	};
	                            	return new Spacebars.SafeString(html);
	                                
	                            }

	                        },
	                        { 
	                            key: '_id', 
	                            label: 'Opciones',
	                            fn: function (_id, object) {
	                                var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat update-auction">Modificar</button><p/><button id="'+_id+'" class="btn btn-info btn-flat btn-flat offers-auction">Ofertas</button> ';
	                                return new Spacebars.SafeString(html);
	                            }

	                        },
	                       
	                    ],
	   
	        };
	    }
	});
}


