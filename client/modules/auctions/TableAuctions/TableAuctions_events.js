if (Meteor.isClient) {
  // counter starts at 0
  Template.TableAuctions.events({
  	'click .update-auction': function (event) {
        event.preventDefault();
        //alert(event.currentTarget.id);
		    Router.go('update-auction',{_Id:event.currentTarget.id});    
    },

    'click .offers-auction': function (event) {
        event.preventDefault();
        //alert(event.currentTarget.id);
        Session.set('auction',event.currentTarget.id);
        Router.go('offer-auction-producer',{_Id:event.currentTarget.id});    
    },
    
  });

  Template.TableAuctions.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    delete Session.keys['auction'];
  });

  Template.TableAuctions.rendered = function(){

  }
}


