if (Meteor.isClient) {
  // counter starts at 0
  	Template.offers_auction.helpers({
  		offers_auction: function(object) {
	    	return  offers.find({auction:Session.get('auction')});
	    },

	    tableSettings : function () {
	         return {
	                    rowsPerPage: 10,
	                    fields: [
	                        { 
	                            key: 'createAt', 
	                            label: 'Fecha' ,

	                        },
	                        { 
	                            key: 'userCreateOffer',
	                            label: 'Nombre-Ofertador' 

	                        },
	                        { 
	                            key: "weight", 
	                            label: 'Peso',
	                            fn: function (offer, object) {
	                                var html =  '<td class="weight">Kg '+offer+'</td>';
	                                return new Spacebars.SafeString(html);
	                            } 
	                        },
	                        { 
	                            key: "offer",
	                            label: 'Oferta',  
	                            fn: function (offer, object) {
	                                var html =  '<td class="offer">$'+offer+'</td>';
	                                return new Spacebars.SafeString(html);
	                            }
	                            

	                        },
	                        { 
	                            key: "status", 
	                            label: 'Estado',
	                            fn: function (status, object) {
	                            	if (estado == "activo"){
	                            		var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
	                            		
	                            	}else if(estado == "finalizada"){
	                            		var html =  '<div id="finalizada" class="btn btn-info btn-sm">Aceptada</div>';
	                            	}else{
	                            		var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
	                            	};
	                            	return new Spacebars.SafeString(html);
	                            }
	                                
	                        },

	                        { 
	                            key: '_id', 
	                            label: 'Opciones',
	                            fn: function (_id, object) {
	                                var html =  '<button id="'+_id+'" class="btn btn-info btn-flat offer-buyer">Ver</button>';
	                                return new Spacebars.SafeString(html);
	                            }

	                        },
	                       
	                    ],
	   
	        };
	    }
  });
}


