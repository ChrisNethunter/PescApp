if (Meteor.isClient) {
	// counter starts at 0
	Template.offer_auction_producer.events({
		'click #cancelar': function () {
			event.preventDefault();
			Router.go('dashboard');    
		},
		'submit #accept-offer': function(event, template) {
			event.preventDefault();
			acceptOffer($('#auction').val(),$("#id-offer").val(),Meteor.userId(),$('#id-buyer').val());
		},
		'click .offer-cancel': function (event) {
	        event.preventDefault();
	        cancelOffer($("#auction").val(),Meteor.userId(),$("#id-offer").val())  
	    },

	});

	Template.offer_auction_producer.onCreated(function () {
	    // Use this.subscribe inside onCreated callback
	    Meteor.subscribe('users');
	    Meteor.subscribe('statusOffers',Meteor.userId());
	});

	Template.offer_auction_producer.rendered = function(){
		setTimeout(function(){
			var idBuyer = $("#id-buyer").val();
		    //Meteor.subscribe('userInfo',idProducer);

		    var dataBuyer = Meteor.users.findOne({_id:idBuyer});


		    $("#cc-buyer").append("C.C "+dataBuyer.profile.cc);
		    $("#phone-buyer").append("Teléfono: "+dataBuyer.profile.phone);
		    $("#address-buyer").append("Direccion: "+dataBuyer.profile.address);
		    $("#email-buyer").append("Email: "+dataBuyer.profile.email);
	

		}, 500);


		setTimeout(function(){ 
		    if(statusOffers.find({auctionId:$("#auction").val(),status:true}).count() == 1){
		    	$(".offer-cancel").fadeIn("slow");
		    }

		}, 1000);
	}

	function acceptOffer(auction,offer,producer,buyer){
		$(".alert").remove()
		if (auction && offer && producer && buyer != ''){
			var date = dateString();
			var dataProducer = Meteor.users.findOne({_id:Meteor.userId()});
			var dataBuyer = Meteor.users.findOne({_id:Meteor.userId()});
			var data = {
				userProducer:Meteor.userId(),
				auctionId:auction,
				offerId:offer,
				producerId:producer,
				buyerId:buyer,
				createAt:date["stringDate"],
				timeCreater:date["time"],
				status:true,
			}
			Meteor.call('offerAccept',data, function(err, res){
				if (err){
					var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas, Intenta de Nuevo</h4>';
					$(liData).appendTo('.alert-offer-insert').fadeIn('slow');
				}else if(res === 2) {
					var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Ya se ha aceptado la oferta</h4>';
					$(liData).appendTo('.alert-offer-insert').fadeIn('slow');
				}else{
					Meteor.call('sendEmailConfirmationOffer',dataProducer, dataBuyer.emails[0].address, function(err, res){
						if (err){
							var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Ha ocurrido un error, Intenta de Nuevo</h4>';
							$(liData).appendTo('.alert-add').fadeIn('slow');
						}else{
							$(".offer-cancel").fadeIn("slow");
							var liData = '\n <h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Oferta Aceptada</h4>\n \
											<p align="center" class="alert">Se ha enviado un correo al comprador para confirmar la compra</p>';
	        				$(liData).appendTo('.alert-offer-insert').slideDown();
						};
					});
				};
				
			});

		};
	}

	function cancelOffer (auctionId,userId,offer){
		$(".alert").remove()
		var date = dateString();
		var dataProducer = Meteor.users.findOne({_id:Meteor.userId()});
		var dataBuyer = Meteor.users.findOne({_id:Meteor.userId()});
		var data = {
					auctionId:auctionId,
					userId:userId,
					offerId:offer,
					dateCancel:date["stringDate"],
					timeCancel:date["time"],
		}
		Meteor.call('offerCancel',data, function(err, res){
			if (err){
				var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas, Intenta de Nuevo</h4>';
				$(liData).appendTo('.alert-offer-insert').fadeIn('slow');
			}else if(res == 2){
				var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Ya se ha cancelado la oferta</h4>';
				$(liData).appendTo('.alert-add').fadeIn('slow');
			}else{
				Meteor.call('sendEmailConfirmationOfferCancel',dataProducer, dataBuyer.emails[0].address, function(err, res){
					if (err){
						var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Ha ocurrido un error, Intenta de Nuevo</h4>';
						$(liData).appendTo('.alert-add').fadeIn('slow');
					}else{
						$(".offer-cancel").fadeOut("slow")
						var liData = '\n <h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Oferta Cancelada</h4>\n \
										<p align="center" class="alert">Se ha enviado un correo al comprador para confirmar la cancelacion</p>';
        				$(liData).appendTo('.alert-offer-insert').slideDown();
					};
				});
			};
			
		});

	} 
}





