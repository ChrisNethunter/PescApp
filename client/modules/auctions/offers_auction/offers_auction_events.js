if (Meteor.isClient) {
  user = Meteor.userId();

  Template.offers_auction.events({
  	'click .offer-buyer': function (event) {
        event.preventDefault();
		    Router.go('offerBuyerInfo',{_Id:event.currentTarget.id});  
    },
  	
  });

  Template.offers_auction.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('offers-auction-producer',Session.get('auction'));

  });


}


