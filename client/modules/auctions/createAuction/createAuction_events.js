if (Meteor.isClient) {
	// counter starts at 0
	Template.createAuction.events({
		'click #cancelar': function () {
			event.preventDefault();
			Router.go('dashboard');    
		},
		'submit #reisterAuction': function(event, template) {
				event.preventDefault();
				createAuction($('#inputDateHarvest').val(),$('#date-initial').val(),$('#date-finish').val(),$('#weight-production').val(),$('#notes-production').val(),$("#value-action").val());
		},
	});

	Template.createAuction.rendered = function(){
	}

}
function createAuction(dateHarvest,dateInit,dateFinish,weight,notes,valueAuction){
	$(".alert").remove();
	if (validate_fecha(dateHarvest) && validate_fecha(dateInit) && validate_fecha(dateFinish) && validateNumber(weight) && isNotEmpty(notes) && validateNumber(valueAuction) === true){

		var currentUserId = Meteor.userId();
		var date = dateString();
		data = {
			createdBy: currentUserId,
			nameCreatedBy: Meteor.user().profile.name,
			dateHarvest:dateHarvest,
			dateInit:dateInit,
			dateFinish:dateFinish,
			value:valueAuction,
			notes:notes,
			weight:weight,
			createAt:date["stringDate"],
			timeCreater:date["time"],
			status:"activo",
		}
		//console.log(JSON.stringify(data));
		Meteor.call('createAuction',data, function(err, res){
			if (err){
				var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Problemas, Intenta de Nuevo</h4>';
				$(liData).appendTo('.alert-add').fadeIn('slow');
			}else{
				var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;" align="center">Subasta Creada</h4>';
				$(liData).appendTo('.alert-add').slideDown();
			};
		});

	}else{
		var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Rellena los Campos Correctamente</h4>';
									$(liData).appendTo('.alert-add').fadeIn('slow');
	};
}




